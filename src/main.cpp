
#include <Arduino.h>
#include <SoftwareSerial.h>


const byte rx_pin = 10;
const byte tx_pin = 11;

boolean acknowledge=true;

float activity[2];
float low_pass[2];
float tau=200;

int ledpin=13;
SoftwareSerial polar_serial =  SoftwareSerial(rx_pin, tx_pin);
String gcode_polar;
String csv_code_debug;
bool stringComplete = false;  // whether the string is complete
String inputString = "";         // a String to hold incoming data

void setup()
{
  Serial.begin(38400);
  polar_serial.begin(9600);
  for (int i = 0; i < 2; i++)
  {
    activity[i] = 0;
  }
  
  polar_serial.println("G0 F10000");

  csv_code_debug= String("");
  Serial.println("homing");
  polar_serial.println("G28 XY");
  delay(5000);
}


void loop()
{
  for (int i = 0; i < 2; i++)
  {
    activity[i] = analogRead(i);
    low_pass[i] += 1 / tau * (-low_pass[i] + activity[i]);
  }
  int x=(int) map(low_pass[0],0,400,-150,150);
  int y=(int) map(low_pass[1],0,400,-150,150);
  //csv_code_debug=x+String(",")+y;
  //Serial.println(csv_code_debug);
  gcode_polar=String("G0 X")+x+String(" Y")+y;
  if (acknowledge) {
    polar_serial.println(gcode_polar);
    acknowledge=false;
     stringComplete = false;
     Serial.println(gcode_polar);
  }
  while (polar_serial.available()) {
    // get the new byte:
    Serial.println("waiting for serial");
    char inChar = (char)polar_serial.read();
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
      return;
    }
  }
  // Serial.println("in main loop");
  inputString.trim();
  if (inputString != "") {
    Serial.println(inputString);
  }
  // ok_str.toLowerCase();
  //  if ((inputString.equals("ok"))||(inputString.equals("mok")))
  //  if ( (inputString.indexOf("ok")>0 ) || ( inputString.equals("ok")) )
  if ((inputString.length()>=1&&inputString.length()<4)||(inputString.indexOf("ok")>0 ))
    {
      Serial.println("ack true");
      acknowledge=true;
      inputString = "";
  } else {
    inputString="";
  }
  
}

/*
  SerialEvent occurs whenever a new data comes in the hardware serial RX. This
  routine is run between each time loop() runs, so using delay inside loop can
  delay response. Multiple bytes of data may be available.
*/
/*void serialEvent() {
   inputString = "";
   while (polar_serial.available()) {
    // get the new byte:
    char inChar = (char)polar_serial.read();
    // add it to the inputString:
    
    inputString += inChar;
    // if the incoming character is a newline, set a flag so the main loop can
    // do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    }
  }
  inputString.trim();
  Serial.println(inputString);
  // ok_str.toLowerCase();
  if ( inputString=="ok")
    {
      acknowledge=true;
      inputString = "";
    }
}
*/
